#[cfg(feature = "progress_through_logs")]
use std::thread::sleep;
#[cfg(feature = "progress_through_logs")]
use std::time::Duration;

use wirelog as log;

fn main() {
    log::init().unwrap();

    // Setting the log level to display all messages
    log::set_level(log::LevelFilter::Trace);

    log::warn!(" -> Setting log level to '{}'", log::level());

    log::trace!("Trace message");
    log::debug!("Debug message");
    log::info!("Info message");
    log::warn!("Warning message");
    log::error!("Error message");

    println!();
    log::set_level(log::LevelFilter::Warn);
    log::warn!(" -> Setting log level to '{}'", log::level());

    log::trace!("Trace message        [Should not be displayed]");
    log::debug!("Debug message        [Should not be displayed]");
    log::info!("Info message          [Should not be displayed]");
    log::warn!("Warning message          [Should be displayed]");
    log::error!("Error message        [Should be displayed]");

    #[cfg(feature = "progress_through_logs")]
    {
        println!();
        log::set_level(log::LevelFilter::Info);
        log::info!("Spinners and stuff!");
        log::info!("[ ] This is a 'ProgressLog':");
        sleep(Duration::from_secs(1));
        log::info!("[ ] You can have multiple progress running at once:");
        sleep(Duration::from_secs(1));
        log::info!("New log messages will be printed above");
        sleep(Duration::from_secs(1));
        log::warn!("ProgressLog updates only concerns the last log");
        sleep(Duration::from_secs(1));
        log::info!("[+] Ending 2nd log, first.");
        sleep(Duration::from_secs(1));
        log::info!("[ ] The 1st log is still running");
        sleep(Duration::from_secs(1));
        log::info!("[+] Ending 1st log, then");

        println!();
        log::info!("[ ] Progress status can be updated:");
        for i in 0..3 {
            log::info!("[ ] {}", (3 - i));
            sleep(Duration::from_millis(500));
        }
        log::info!("[-] Progress can also fail");

        log::info!("[+] Success / Failures logs can be used without progress");

        println!();
        log::set_progress_static(true);
        log::info!("[ ] There is also some kind of static progress logs:");
        log::info!("In this case, other logs are classically printed after");
        for i in 0..3 {
            log::info!("[ ] {}", 3 - i);
            if i == 1 {
                log::warn!("This kind of log might be useful for interactive applications");
            }
            sleep(Duration::from_millis(750));
        }
        log::info!("[+] Done");
    }
}
