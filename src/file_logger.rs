/*
 * Copyright (C) See full copyright notice in the COPYRIGHT file
 * at the top-level directory of the project.
 *
 * This file is part of 'wirelog'.
 *
 *   Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
 *   http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
 *   <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
 *   option. This file may not be copied, modified, or distributed
 *   except according to those terms.
 *
 */

//! Manage file logging.

use std::io::Write;
use std::time::SystemTime;

use std::path::PathBuf;
use std::fs::{File, OpenOptions};
use std::sync::Mutex;

use log::{Metadata, Record};

/// Logger used to output logs in a given file with a given `LevelFilter`.
pub struct FileLogger {
    start_time:     SystemTime,
    file:           Mutex<File>,
    level_filter:   log::LevelFilter,
}

impl FileLogger {
    /// Creates a `FileLogger` which will open the given file
    /// for appending any output logs allowed by the given level_filter.
    pub fn new(filepath: PathBuf, level_filter: log::LevelFilter) -> Self {
        let start_time = SystemTime::now();
        let file = OpenOptions::new()
                                .append(true)
                                .create(true)
                                .open(filepath)
                                .expect("Could not open log file");
        let file = Mutex::new(file);

        FileLogger {
            start_time,
            file,
            level_filter,
        }
    }
}

impl log::Log for FileLogger {
    fn enabled(&self, metadata: &Metadata) -> bool {
        metadata.level() <= self.level_filter
    }

    fn log(&self, record: &Record) {
        if !self.enabled(record.metadata()) {
            return;
        }

        let timestamp = self.start_time.elapsed().unwrap();

        let mut file = self.file.lock().unwrap();
        writeln!(file, "[{:>9.3} | {} ] {:^5}: {}",
               timestamp.as_secs_f32(),
               record.target(),
               record.level(),
               record.args()
            ).unwrap();
    }

    fn flush(&self) {
        let mut file = self.file.lock().unwrap();
        file.flush().expect("Could not flush file");
    }
}

