/*
 * Copyright (C) See full copyright notice in the COPYRIGHT file
 * at the top-level directory of the project.
 *
 * This file is part of 'wirelog'.
 *
 *   Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
 *   http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
 *   <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
 *   option. This file may not be copied, modified, or distributed
 *   except according to those terms.
 *
 */

//! Progress logs with spinners.
//!
//! This module encapsulate the progress logs logic.
//! It defines some functions to manipulate progress logs.
//!

use std::fmt::Display;
use std::sync::Mutex;

use crate::Level as LogLevel;

mod progress_logger;
use progress_logger::ProgressLogger;

mod progress_display;
use progress_display::{State,
                        ProgressDisplay,
                        ProgressDisplayDynamic,
                        ProgressDisplayStatic};

/// The global object displaying for progress logs.
static mut LOGGER: Option<Mutex<ProgressLogger>> = None;

/// Should be called before being able to print logs.
///
/// If the function is not called, other functions from the module
/// will at best do nothing, at worst cause panics.
pub fn init() {
    unsafe {
        LOGGER = Some(Mutex::new(ProgressLogger::new(false)));
    }
}

/// Change the display of the progress logs.
///
/// (See [Static Progress logs](crate#static-progress-log))
///
/// This function should be called when no progress logs
/// are running.
pub fn set_static(static_logs: bool) {
    unsafe {
        if let Some(logger) = &mut LOGGER {
            logger.get_mut().unwrap().set_static(static_logs);
        }
    }
}

/// Starts a progress new log with the given message.
pub fn progress<T: Display>(level: LogLevel, msg: T) {
    run(level, msg, State::Running);
}

/// Returns `true` if a progress log is currently running.
pub fn is_running() -> bool {
    unsafe {
        if let Some(logger) = &LOGGER {
            logger.lock().unwrap().is_running()
        } else {
            false
        }
    }
}

/// Print a line above a running progress.
///
/// Panics if no logs are currently running.
pub(super) fn print_line_above(line: String) {
    unsafe {
        if let Some(logger) = &LOGGER {
            let logger = logger.lock().unwrap();

            if logger.is_running() {
               logger.print_regular_log(line);
            } else {
                panic!("No Progress log running");
            }
        }
    }
}


/// Update the status of a running progress log.
///
/// If no progress are running, the function does nothing.
pub fn status<T: Display>(msg: T) {
    unsafe {
        if let Some(logger) = &mut LOGGER {
            let logger = logger.get_mut().unwrap();

            if logger.is_running() {
                logger.status(msg);
            }
        }
    }
}

/// If a progress is running, terminates it with a success state
/// and update its status with the given message.
///
/// If no progress are running,
/// just print a success log with the given message.
pub fn success<T: Display>(level: LogLevel, msg: T) {
    unsafe {
        if let Some(logger) = &mut LOGGER {
            let logger = logger.get_mut().unwrap();

            if logger.is_running() {
                logger.success(msg);
            } else {
                //drop(logger); // Unlock

                run(level, msg, State::Success);
            }
        }
    }
}

/// If a progress is running, terminates it with a failure state
/// and update its status with the given message.
///
/// If no progress are running,
/// just pring a failure log with the given message.
pub fn failure<T: Display>(level: LogLevel, msg: T) {
    unsafe {
        if let Some(logger) = &mut LOGGER {
            let logger = logger.get_mut().unwrap();

            if logger.is_running() {
                logger.failure(msg);
            } else {
                //drop(logger); // Unlock

                run(level, msg, State::Failure);
            }
        }
    }
}

/// Terminates a running progress with a success state.
///
/// Does nothing if no logs are running.
pub fn done() {
    unsafe {
        if let Some(logger) = &mut LOGGER {
            let logger = logger.get_mut().unwrap();

            if logger.is_running() {
                logger.success("Done");
            }
        }
    }
}

/// Terminates a running progress with a failure state.
///
/// Does nothing if no logs are running.
pub fn fail() {
    unsafe {
        if let Some(logger) = &mut LOGGER {
            let logger = logger.get_mut().unwrap();

            if logger.is_running() {
                logger.failure("Failed");
            }
        }
    }
}

/// Starts a ProgressLog with the given message and the given state
fn run<T: Display>(level: LogLevel, msg: T, state: State) {
    unsafe {
        if let Some(logger) = &mut LOGGER {
            logger.get_mut().unwrap().log(level, msg, state);
        }
    }
}
