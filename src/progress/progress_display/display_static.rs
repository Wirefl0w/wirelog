/*
 * Copyright (C) See full copyright notice in the COPYRIGHT file
 * at the top-level directory of the project.
 *
 * This file is part of 'wirelog'.
 *
 *   Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
 *   http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
 *   <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
 *   option. This file may not be copied, modified, or distributed
 *   except according to those terms.
 *
 */

//! The static display style of progress logs.
//!
use crate::terminal::Colorize;
use crate::level as log_level;

use super::{State,
            ProgressLog,
            ProgressDisplay};

use super::{
    SUCCESS,
    FAILURE
};

static STATIC_SPIN: &str = "0"; // The char of the static spinner

/// In charge of displaying the progress logs.
pub(crate) struct ProgressDisplayStatic {
    logs:       Vec<ProgressLog>,           // The list of current running logs.
    status_msg: String,                     // The status of the log

    prefix:         String,                 // The static progress log prefix.
    prefix_success: String,                 // The character prefix for a success.
    prefix_failure: String,                 // The character prefix for a failure.
}


impl ProgressDisplayStatic {
    /// Create an empty ProgressDisplay.
    pub fn new() -> Self {
        ProgressDisplayStatic {
            logs: Vec::new(),
            status_msg: String::new(),
            prefix:             STATIC_SPIN.purple(),
            prefix_success:     SUCCESS.green(),
            prefix_failure:     FAILURE.red(),
        }
    }
}


impl ProgressDisplay for ProgressDisplayStatic {
    fn log(&mut self, level: log::Level, msg: &str, state: State) {
        self.status_msg = String::new();

        let log = ProgressLog {
            level,
            state,
            msg: msg.to_string(),
        };


        self.logs.push(log);
        self.draw();
    }

    fn is_running(&self) -> bool {
        ! self.logs.is_empty()
    }

    fn is_static(&self) -> bool {
        true
    }


    fn status(&mut self, status: &str) {
        self.status_msg = status.to_string();
        self.draw();
    }

    fn success(&mut self, status: &str) {
        if let Some(log) = self.logs.last_mut() {
            self.status_msg = status.to_string();
            log.state = State::Success;
            self.draw();
        }
    }

    fn failure(&mut self, status: &str) {
        if let Some(log) = self.logs.last_mut() {
            self.status_msg = status.to_string();
            log.state = State::Failure;
            self.draw();
        }
    }

    /// Static case: Just print the line.
    fn print_regular_log(&self, line: &str) {
        println!("{}", line);
    }

    fn draw(&mut self) -> bool {
        // Drop logs for which the current log level is filtering them
        let level = log_level();
        self.logs.retain(|log| log.level <= level);

        // Print last log, if any and retrieve the information
        // that the last log just terminated (success or failure).
        let last_log_terminated = if let Some(log) = self.logs.last_mut() {
            let prefix = match log.state {
                State::Running => &self.prefix,
                State::Success => &self.prefix_success,
                State::Failure => &self.prefix_failure,
                State::Stopped => unreachable!(),
            };

            print!("[{}] {}", prefix, log.msg);

            if !self.status_msg.is_empty() {
                println!(": {}", self.status_msg);
            } else {
                println!();
            }

            log.state != State::Running
        } else {
            false
        };

        // Drop the last log if it has terminated.
        if last_log_terminated {
            self.logs.pop();
        }

        self.is_running()
    }
}

