/*
 * Copyright (C) See full copyright notice in the COPYRIGHT file
 * at the top-level directory of the project.
 *
 * This file is part of 'wirelog'.
 *
 *   Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
 *   http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
 *   <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
 *   option. This file may not be copied, modified, or distributed
 *   except according to those terms.
 *
 */

//! Display of the progress logs in the terminal.
//!
//! This module manages the displaying of the logs in the terminal
//! in both the normal (dynamic logs with a running spinner) and the
//! static display style.
//!

use std::marker::Send;
use crate::Level as LogLevel;

mod display_dynamic;
mod display_static;

pub(super) use display_dynamic::ProgressDisplayDynamic;
pub(super) use display_static::ProgressDisplayStatic;

static FAILURE: &str = "-"; // The Failure char
static SUCCESS: &str = "+"; // The Success char

/// Represents the state of a progress spinner
#[derive(Clone, Copy, PartialEq)]
pub(super) enum State {
    /// The log is running, spinner should be spinning.
    Running,
    /// The log is stopped, so is the spinner.
    Stopped,
    /// The log has finished as a success.
    Success,
    /// The log has finished as a failure.
    Failure,
}

/// Represents the actual progress log.
struct ProgressLog {
    state:  State,      // The current state of the progress log.
    level:  LogLevel,   // The level of the progress log.
    msg:    String,     // The message of the log.
}

pub(super) trait ProgressDisplay: Send {
    /// Start displaying a new running progress logs.
    ///
    /// Adds a new log to the list of running logs and draws it.
    fn log(&mut self, level: log::Level, msg: &str, state: State);

    /// Tells if the log is running.
    ///
    /// A [`ProgressLog`] is considered running if it might need to be
    /// drawn in the future (for a status or a state update).
    fn is_running(&self) -> bool;

    /// Tells if the display is static (i.e outputing static progress logs).
    fn is_static(&self) -> bool;

    /// Update the status of the `ProgressLog` and draws it.
    fn status(&mut self, status: &str);
    /// Terminate the log with a success state and a final status,
    /// then draws it.
    fn success(&mut self, status: &str);
    /// Terminate the log with a failure state and a final status,
    /// then draws it.
    fn failure(&mut self, status: &str);


    /// Print a regular log line along with the current running
    /// progress logs.
    fn print_regular_log(&self, line: &str);

    /// Draws the `ProgressLog`
    ///
    /// The function returns a boolean indicating if the log is still running
    /// after the call.
    fn draw(&mut self) -> bool;
}
