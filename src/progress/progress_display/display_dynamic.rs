/*
 * Copyright (C) See full copyright notice in the COPYRIGHT file
 * at the top-level directory of the project.
 *
 * This file is part of 'wirelog'.
 *
 *   Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
 *   http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
 *   <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
 *   option. This file may not be copied, modified, or distributed
 *   except according to those terms.
 *
 */

//! The normal (dynamic) progress log display style with spinners.
//!
use crate::level as log_level;

use super::{State,
            ProgressLog,
            ProgressDisplay};

use crate::terminal::{Colorize, move_up, ERASE_LINE};

use super::{
    SUCCESS,
    FAILURE
};
static SPIN_CHARS: [&str; 4] = [&"|", &"/", &"-", &"\\"]; // The chars of the animated spinner

/// In charge of displaying the progress logs.
pub(crate) struct ProgressDisplayDynamic {
    logs:       Vec<ProgressLog>,           // The list of current running logs.
    status_msg: String,                     // The status of the log

    prefix_spin_chars: Vec<String>,         // The characters of the spinner.
    spin_idx:   usize,                      // The current index of the spin_chars.
    prefix_success: String,                 // The character prefix for a success.
    prefix_failure: String,                 // The character prefix for a failure.
}

impl ProgressDisplayDynamic {
    /// Initialize an empty [`ProgressDisplayDynamic`]
    pub fn new() -> Self {
        let prefix_spin_chars: Vec<String> = SPIN_CHARS.iter().map(|x| x.blue()).collect();

        ProgressDisplayDynamic {
            logs: Vec::new(),
            status_msg: String::new(),
            prefix_spin_chars,
            spin_idx: 0,
            prefix_success:     SUCCESS.green(),
            prefix_failure:     FAILURE.red(),
        }
    }


    /// Move the cursor to the beginning of the progress logs
    fn move_to_begin(&self) {
        if !self.logs.is_empty() {
            print!("{}\r", move_up(self.logs.len()));
        }
    }

    /// Moves the cursor up to the first log line and clears it.
    fn clear_first_line(&self) {
        self.move_to_begin();
        print!("{}\r", ERASE_LINE);
    }

    /// Draw all the logs as is.
    ///
    /// The function just print all the logs in the list, regardless of their state
    /// and without updating the spinner.
    fn redraw(&self) {
        if let Some(last_log) = self.logs.last() {
            // Display all other running logs first.
            for log in self.logs.iter().take(self.logs.len() - 1) {
                println!("{}[ ] {}", ERASE_LINE, log.msg);
            }

            print!("{}", ERASE_LINE);
            // Print the spinner with the last message.
            print!("[{}] {}", SPIN_CHARS[self.spin_idx].blue(), last_log.msg);

            // Add the status, if any.
            if !self.status_msg.is_empty() {
                println!(": {}", self.status_msg);
            } else {
                println!();
            }
        }
    }
}


impl ProgressDisplay for ProgressDisplayDynamic {
    fn log(&mut self, level: log::Level, msg: &str, state: State) {
        self.spin_idx = 0;
        self.status_msg = String::new();

        let log = ProgressLog {
            level,
            state,
            msg: msg.to_string(),
        };


        // Drawing logs starts by erasing the current logs.
        // So, add a newline for the new added log.
        println!();

        self.logs.push(log);
        self.draw();
    }

    fn is_running(&self) -> bool {
        ! self.logs.is_empty()
    }

    fn is_static(&self) -> bool {
        false
    }


    fn status(&mut self, status: &str) {
        self.status_msg = status.to_string();
        self.draw();
    }

    fn success(&mut self, status: &str) {
        if let Some(log) = self.logs.last_mut() {
            self.status_msg = status.to_string();
            log.state = State::Success;
            self.draw();
        }
    }

    fn failure(&mut self, status: &str) {
        if let Some(log) = self.logs.last_mut() {
            self.status_msg = status.to_string();
            log.state = State::Failure;
            self.draw();
        }
    }

    /// Dynamic case: Print the line above
    /// the current running progress logs.
    fn print_regular_log(&self, line: &str) {
        self.clear_first_line();
        println!("{}", line);
        self.redraw();
    }


    fn draw(&mut self) -> bool {
        // Stop logs for which the current log level is filtering them
        let level = log_level();
        for log in self.logs.iter_mut() {
            if level < log.level {
                log.state = State::Stopped;
            }
        }

        // Retrieve non running logs
        let non_running_logs = self.logs.iter()
            .filter(|log| log.state != State::Running);

        self.move_to_begin();

        // Print non running logs
        for log in non_running_logs {
            print!("{}", ERASE_LINE);
            let prefix = match log.state {
                State::Success => &self.prefix_success,
                State::Failure => &self.prefix_failure,
                State::Stopped => &self.prefix_spin_chars[self.spin_idx],
                _ => unreachable!(),
            };

            print!("[{}] {}", prefix, log.msg);

            if log.state != State::Stopped && !self.status_msg.is_empty() {
                // Proper log ending.
                println!(": {}", self.status_msg);
                // Clears status message for next last log
                self.status_msg.clear();
            } else {
                // Log is interrupted.
                println!();
            }
        }

        // Only keep running logs.
        self.logs.retain(|log| log.state == State::Running);

        self.redraw();

        self.spin_idx = (self.spin_idx + 1) % self.prefix_spin_chars.len();

        self.is_running()
    }
}

