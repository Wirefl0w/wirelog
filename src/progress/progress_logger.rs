/*
 * Copyright (C) See full copyright notice in the COPYRIGHT file
 * at the top-level directory of the project.
 *
 * This file is part of 'wirelog'.
 *
 *   Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
 *   http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
 *   <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
 *   option. This file may not be copied, modified, or distributed
 *   except according to those terms.
 *
 */

//! This module implements the `ProgressLogger` struct.
//!
use std::fmt::Display;
use std::sync::{Arc, Mutex};
use std::thread;
use std::time::Duration;

use super::{ProgressDisplay,
            ProgressDisplayStatic,
            ProgressDisplayDynamic};

use super::State;

static DRAW_INTERVAL: Duration = Duration::from_millis(150); // The duration between two spinner frames

/// The `ProgressLogger` is in charge of managing progress logs.
///
/// This structure is a wrapper around the `ProgressDisplay`
/// It shares an `Arc<Mutex<ProgressDisplay>>` with a thread
/// that periodically draws the log to update the spinner
pub(super) struct ProgressLogger {
    display:        Arc<Mutex<dyn ProgressDisplay>>,
    static_logs:    bool,
    drawer:         Option<thread::JoinHandle<()>>,
}


impl ProgressLogger {
    /// Constructor for a new `ProgressLogger`
    pub fn new(static_logs: bool) -> Self {
        let display: Arc<Mutex<dyn ProgressDisplay>> = if static_logs {
            Arc::new(Mutex::new(ProgressDisplayStatic::new()))
        } else {
            Arc::new(Mutex::new(ProgressDisplayDynamic::new()))
        };

        ProgressLogger {
            display,
            static_logs,
            drawer: None
        }
    }

    pub fn set_static(&mut self, static_logs: bool) {
        if self.static_logs != static_logs {
            if self.is_running() {
                panic!("Display style of progress logs can only be changed if no progress logs are running");
            }
            *self = ProgressLogger::new(static_logs);
        }
    }


    /// Log a new progress message.
    ///
    /// If another progress log was running, the new log replaces it.
    pub fn log<T: Display>(&mut self, level: log::Level, msg: T, state: State) {
        // Drop previous drawer thread if it has finished.
        // If the thread is still running, it will be reused for next progress
        // if needed.
        if let Some(handle) = self.drawer.take() {
            if handle.is_finished() {
                drop(handle);
            } else {
                self.drawer = Some(handle);
            }
        }

        {
            // Creates the progress log and displays it.
            let mut display = self.display.lock().unwrap();
            display.log(level, &msg.to_string(), state);
        }

        // Start the drawing thread if the log is running.
        // (and we don't already have a drawing thread running).
        if state == State::Running && self.drawer.is_none() {
            let shared_display = self.display.clone();
            self.drawer = Some(
                thread::spawn(move || {
                    let mut running = true;
                    while running {
                        thread::sleep(DRAW_INTERVAL);
                        let mut display = shared_display.lock().unwrap();
                        running = if display.is_static() {
                            // Avoid drawing the log if the log
                            // has been terminated and a new static
                            // log has been created during the sleeping time
                            false
                        } else {
                            display.draw()
                        };
                    }
                })
            );
        }
    }

    /// Tells if the progress is currently running.
    pub fn is_running(&self) -> bool {
        let display = self.display.lock().unwrap();
        display.is_running()
    }

    /// Print a regular log line.
    ///
    /// In the case of a running dynamic log, it will print the line above.
    /// In the case of a static log, it just print the line.
    pub fn print_regular_log<T: Display>(&self, line: T) {
        let display = self.display.lock().unwrap();
        display.print_regular_log(&line.to_string());
    }

    /// Updates the status of the progress log
    pub fn status<T: Display>(&mut self, status: T) {
        let mut display = self.display.lock().unwrap();
        display.status(&status.to_string());
    }

    /// Terminates the log with a success state and the given
    /// final status.
    pub fn success<T: Display>(&mut self, status: T) {
        let mut display = self.display.lock().unwrap();
        display.success(&status.to_string());
    }

    /// Terminates the log with a failure state and the given
    /// final status.
    pub fn failure<T: Display>(&mut self, status: T) {
        let mut display = self.display.lock().unwrap();
        display.failure(&status.to_string());
    }
}

impl Drop for ProgressLogger {
    fn drop(&mut self) {
        if let Some(handle) = self.drawer.take() {
            drop(handle);
        }
    }
}

