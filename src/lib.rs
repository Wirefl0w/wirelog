/*
 * Copyright (C) See full copyright notice in the COPYRIGHT file
 * at the top-level directory of the project.
 *
 * This file is part of 'wirelog'.
 *
 *   Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
 *   http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
 *   <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
 *   option. This file may not be copied, modified, or distributed
 *   except according to those terms.
 *
 */

#![doc = include_str!("../README.md")]

use std::collections::HashSet;
use std::sync::Mutex;

use log::{Record, Metadata};

pub use log::{log, trace, debug, info, warn, error,
log_enabled};
pub use log::{Level, LevelFilter};

mod terminal;
mod config;
mod file_logger;

#[cfg(feature = "progress")]
mod progress;

use terminal::Colorize;
pub use config::LoggingConfig;
use file_logger::FileLogger;

#[cfg(feature = "progress")]
pub use progress::{progress,
                    status, success, failure,
                    done, fail};

#[cfg(feature = "progress")]
pub use progress::set_static as set_progress_static;

/// The console level filter, used when file logging is enabled.
///
/// If file logging is enabled, the `max_level` for the `log` crate
/// is set as the maximum between this `LEVEL_FILTER` and the
/// file logging `LevelFilter` (so all necessary logs are taken
/// into account).
///
/// When file logging is disabled, the console level (the only
/// one in use), is the same as `log::max_level`.
static mut LEVEL_FILTER: Mutex<log::LevelFilter> = Mutex::new(log::LevelFilter::Info);

/// Tells the `LevelFilter` of the file logging if enabled.
static mut FILE_LOGGING_LEVEL: Option<log::LevelFilter> = None;

/// Initialize the library.
///
/// This function should be called first, before
/// any message can be logged using `log` the macros.
///
/// **The initialization function can be called only
/// once.**
pub fn init() -> Result<(), log::SetLoggerError> {
    init_with_config(LoggingConfig::default())
}

/// Initialize the library with the given config.
fn init_with_config(config: LoggingConfig) -> Result<(), log::SetLoggerError> {
    if config.filepath.is_some() {
        unsafe { FILE_LOGGING_LEVEL = Some(config.file_level_filter); }
    }

    // FIXME: Awaiting for const argument formatting in stable Rust,
    // currently using the `std` feature of the `log` crate
    // to be able to build the string prefixes (and avoid
    // recomputing them each time we print something).
    let logger = Logger::new(config);
    log::set_boxed_logger(Box::new(logger))?;

    set_level(log::LevelFilter::Info);

    #[cfg(feature = "progress")]
    progress::init();

    Ok(())
}

/// Returns a [`LoggingConfig`] object which allows
/// to configure the library.
pub fn config() -> LoggingConfig {
    LoggingConfig::default()
}

/// Returns the current maximum log level for the console output.
///
/// All logs with an associated level above the maximum log level
/// will be discared.
pub fn level() -> log::LevelFilter {
    let file_logging_level = unsafe { FILE_LOGGING_LEVEL };

    if file_logging_level.is_some() {
        unsafe { *LEVEL_FILTER.lock().unwrap() }
    } else {
        log::max_level()
    }
}

/// Sets the global maximum log level for the console output.
///
/// All logs with an associated level above the maximum log level
/// will be discared.
pub fn set_level(level: log::LevelFilter) {
    let file_logging_level = unsafe { FILE_LOGGING_LEVEL };

    if let Some(file_level) = file_logging_level {
        unsafe { *LEVEL_FILTER.lock().unwrap() = level; }
        log::set_max_level(std::cmp::max(level, file_level));
    } else {
        log::set_max_level(level);
    }
}

/// The logger passed to the `log` library.
///
/// Holds the different prefixes for log levels,
/// and the logging configuration.
struct Logger {
    // log prefixes
    trace: String,
    debug: String,
    info:  String,
    warn:  String,
    error: String,

    module_filter: HashSet<String>,

    file_logger: Option<FileLogger>,
}


impl Logger {
    /// Create a new [`Logger`] with the given [`LoggingConfig`].
    fn new(config: LoggingConfig) -> Self {
        let filelogger = if let Some(filepath) = config.filepath {
            Some(FileLogger::new(filepath, config.file_level_filter))
        } else {
            None
        };

        Logger {
            trace   : "i".green(),
            debug   : "debug".on_green(),
            info    : "*".blue(),
            warn    : "!".yellow(),
            error   : "ERROR".red(),

            module_filter: config.module_filter,

            file_logger: filelogger,
        }
    }

    /// Checks if the given module is allowed to log.
    fn module_allowed(&self, module: &str) -> bool {
        if !self.module_filter.is_empty() {
            self.module_filter.iter().any(|f| module.starts_with(f))
        } else {
            // If empty, no filters applied: logs allowed from everywhere.
            true
        }
    }

    /// Check if the level filtering allows the given record metadata
    /// for the **console** logging.
    #[inline]
    fn level_enabled(metadata: &Metadata) -> bool {
        metadata.level() <= level()
    }
}

impl log::Log for Logger {
    /// Returns `true` if the log is enabled for either
    /// the stdout logging or file logging.
    fn enabled(&self, metadata: &Metadata) -> bool {
        metadata.level() <= log::max_level() &&
            self.module_allowed(metadata.target())
    }

    fn log(&self, record: &Record) {
        if !self.enabled(record.metadata()) {
            return;
        }

        // File logging (level filtering might be at a more verbose
        // level than console logging).
        if let Some(logger) = &self.file_logger {
            logger.log(record);
        }

        // Display the log on stdout if console level enables it.
        if !Self::level_enabled(record.metadata()) {
            return;
        }

        #[cfg(feature = "progress_through_logs")]
        if progress_log(record.level(), &format!("{}", record.args())) {
            return;
        }

        let prefix = match record.level() {
            Level::Trace    => &self.trace,
            Level::Debug    => &self.debug,
            Level::Info     => &self.info,
            Level::Warn     => &self.warn,
            Level::Error    => &self.error
        };
        let line = format!("[{}] {}", prefix, record.args());

        #[cfg(feature = "progress")]
        if progress::is_running() {
            progress::print_line_above(line);
            return;
        }

        println!("{}", line);
    }

    fn flush(&self) {}
}

/// Checks whether the given log message corresponds to
/// a progress log message and logs it in this case.
///
/// See 'Progress log through log macros' section
/// of the lib documentation for more details.
///
/// Returns true if the message was matching the progress log format.
#[cfg(feature = "progress_through_logs")]
fn progress_log(level: Level, msg: &str) -> bool {
    let mut matched_progress_log = false;

    // Progress logs match '^[.] '
    // Not sure this is the most optimal way to check.
    if msg.len() > 4 && msg.starts_with('[') && msg[2..].starts_with("] ") {
        matched_progress_log = true;

        match &msg[1..2] {
            " " => {
                if msg.ends_with(':') {
                    // Create a new progress log
                    progress(level, msg[4..].strip_suffix(':').unwrap());
                } else {
                    // Update the status
                    status(&msg[4..]);
                }
            },
            "+" => success(level, &msg[4..]),
            "-" => failure(level, &msg[4..]),
            _ => matched_progress_log = false, // Not a progress log
        }
    }

    matched_progress_log
}

