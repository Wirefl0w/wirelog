/*
 * Copyright (C) See full copyright notice in the COPYRIGHT file
 * at the top-level directory of the project.
 *
 * This file is part of 'wirelog'.
 *
 *   Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
 *   http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
 *   <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
 *   option. This file may not be copied, modified, or distributed
 *   except according to those terms.
 *
 */

//! Manage logging configuration.

use std::collections::HashSet;
use std::path::PathBuf;

/// Configure the logging system.
///
/// It allows to filter which module will be allowed to emit log messages.
/// By default, *no filters are applied*, so any module as well as any dependencies
/// of the crate initializing the library will produce log messages using
/// the `log::*!` macros.
///
/// Using the [`allow_module`](LoggingConfig::allow_module)
/// builder method add filters for which module
/// will be allowed to log.
///
/// A log file can be specified with an associated level filtering,
/// where messages will be written in addition to the logs on screen.
///
pub struct LoggingConfig {
    pub(crate) filepath:            Option<PathBuf>,
    pub(crate) file_level_filter:   log::LevelFilter,
    pub(crate) module_filter:       HashSet<String>,
}

impl LoggingConfig {
    /// Outputs the log message into the given file,
    /// with the given level filtering (which cannot be changed). \
    /// _The file logging takes a different `LevelFilter` but
    /// uses the same module whitelist._
    ///
    /// If the file exists, new logs will be appended at the end.
    ///
    /// The program panics if the file cannot be opened.
    pub fn log_file<T: Into<PathBuf>>(mut self, path: T, level: log::LevelFilter) -> Self {
        self.filepath = Some(path.into());
        self.file_level_filter = level;

        self
    }

    /// Adds a new module to the whitelist.
    ///
    /// If the full module path starts with the given `module` argument,
    /// it will be allowed to log. Therefore, if you allow `module::submodule`,
    /// any log emitted by `module::submodule` **as well as all
    /// its submodules**, will be logged.
    pub fn allow_module(mut self, module: &str) -> Self {
        self.module_filter.insert(module.to_string());

        self
    }

    /// Initialize the library with the given config.
    pub fn init(self) -> Result<(), log::SetLoggerError> {
        super::init_with_config(self)
    }

}

impl Default for LoggingConfig {
    fn default() -> Self {
        LoggingConfig {
            filepath:           None,
            file_level_filter:  log::LevelFilter::Off,
            module_filter:      HashSet::new(),
        }
    }
}

