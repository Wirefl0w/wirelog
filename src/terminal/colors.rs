/*
 * Copyright (C) See full copyright notice in the COPYRIGHT file
 * at the top-level directory of the project.
 *
 * This file is part of 'wirelog'.
 *
 *   Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
 *   http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
 *   <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
 *   option. This file may not be copied, modified, or distributed
 *   except according to those terms.
 *
 */

//! Color formatting for terminal output.
//!
//! This module contains ANSI escape sequences to display
//! color in VT-100 capable terminals.
//!

use std::fmt::Display;

/// FIXME: Wait for concatenation of `static &str` at const level to be stable
/// to build escape sequences.

/// Restore Default formatting
static DEF: &str =       "\x1b[0m";

// Text colors
static RED: &str =       "\x1b[31m";
static GREEN: &str =     "\x1b[32m";
static YELLOW: &str =    "\x1b[33m";
static BLUE: &str =      "\x1b[34m";
static PURPLE: &str =    "\x1b[35m";

// Background colors
static ON_GREEN: &str =  "\x1b[42m";


impl<T: Display> Colorize for T {
    fn colorize(&self, fmt: &'static str) -> String {
        let self_str = format!("{}", self);

        if self_str.ends_with(DEF) {
            // Do not add more DEF escape
            // if there is already one.
            format!("{}{}", fmt, self)
        } else {
            format!("{}{}{}", fmt, self, DEF)
        }
    }
}

/// Define methods to decorate text.
pub trait Colorize {
    /// Concatenate the given fmt, self
    /// and the Default format escape sequence
    /// (if self doesn't already ends with one).
    fn colorize(&self, fmt: &'static str) -> String;

    fn red(&self) -> String {
        self.colorize(RED)
    }

    fn green(&self) -> String {
        self.colorize(GREEN)
    }

    fn yellow(&self) -> String {
        self.colorize(YELLOW)
    }

    fn blue(&self) -> String {
        self.colorize(BLUE)
    }

    fn purple(&self) -> String {
        self.colorize(PURPLE)
    }



    fn on_green(&self) -> String {
        self.colorize(ON_GREEN)
    }
}

