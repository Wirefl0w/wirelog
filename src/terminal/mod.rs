/*
 * Copyright (C) See full copyright notice in the COPYRIGHT file
 * at the top-level directory of the project.
 *
 * This file is part of 'wirelog'.
 *
 *   Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
 *   http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
 *   <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
 *   option. This file may not be copied, modified, or distributed
 *   except according to those terms.
 *
 */

//! Terminal related functions using ANSI escape sequences
//!

pub mod colors;

pub use colors::Colorize;

pub static ERASE_LINE: &str = "\x1b[2K";

#[inline]
pub fn move_up(lines: usize) -> String {
    format!("\x1b[{}A", lines)
}

