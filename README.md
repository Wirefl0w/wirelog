# `wirelog`

Small logging library with colorful prefixes and spinners.

The look and feel is inspired by the `log` module of
[`pwntools`](https://github.com/Gallopsled/pwntools)
but using the `Rust` logging API (which uses different levels).

This library tries to keep things quite simple and minimal
and is not at a very advanced stage of developement,
therefore it might not suit every need (or platform).

Better logging libraries (well-known, tested and established) can be found
[in the `log` crate docs](https://docs.rs/log/latest/log/#available-logging-implementations)
.

## Usage

```rust
// The `log` API is available in the `wirelog` crate
// therefore, here is a quickstart way of using the lib:
use wirelog as log;

fn main() {
    // The library must be initialized first.
    log::init().unwrap();

    // The `log` macros are then, available
    log::info!("Start logging");

    // `set_level()` & `level()` functions to manage
    // the logging level.
    log::set_level(log::LevelFilter::Debug);
    log::warn!("Setting the log level to {}", log::level());
}
```
### Configuration

The [`config()`] functions allows to configure the library through
the [`LoggingConfig`] `struct` using a builder pattern.

```rust no_run
use wirelog as log;

fn main() {
    // Assuming the current crate is named `this_binary`,
    // the following configuration allows all the log messages
    // within the crate to be logged as well as all logs emitted
    // by the `some_module` mod (including all submodules) of
    // the `some_dependency` crate.
    //
    // Moreover, the logs up to the `Debug` level
    // will be written in the file `/tmp/wirelog_doc.log`.
    log::config()
        .log_file("/tmp/wirelog_doc.log", log::LevelFilter::Debug)
        .allow_module("this_binary")
        .allow_module("some_dependency::some_module")
        .init().unwrap();
}
```

_There is currently no way to change the configuration after the
initialization (including the file logging `LevelFilter`)._

### File logging

The library allows to log messages into a file with a given `LevelFilter`
different from the one used to display message in stdout. The `LevelFilter`
for the file logging is set at configuration and cannot be changed afterward.

Currently, the log format is not customizable for file logging:

`[timestamp | module ] LEVEL: message`

The timestamp is the number of seconds (with milliseconds decimals)
from the approximate time the library was initialized.

## The progress log
_`(feature = "progress")`_

The progress log is a log message indicating a running action.
Its prefix is a running spinner and a status suffix can be
updated.

Progress logs can be started using the [`progress()`] function.

Multiple progress logs can be running at the same time (after calling
[`progress()`] multiple times). In this case, only the last started
progress logs is concerned by the updates.

New regular logs can be printed, but they will be printed above the
running progress. All the running progress logs will remain the last
lines on the screen.

When a progress log is running, the [`status`] function can be used to update
its status.
_(When multiple progress logs are running, it only update
the status of the last created log)._

If the log level is changed to a higher level than a running progress, the log
will be discarded. The spinner stops being updated and the line remains as its last
printed form. (When multiple progress logs are running, the discarded line will
be printed above the remaining progress logs).

Progress logs can be stopped with the [`success()`] and [`failure()`] functions
which will show different end prefix.
These functions take an extra argument which will be printed as a final status.
_(When multiple logs are running, these functions only affects the last
created log)._

_Moreover, the functions [`done()`] and [`fail()`] doesn't take any argument
and display a default end status._

Note that, the functions for ending a progress can be called
without a progress running. It will just print the given message
with the appropriate prefix.

### Static progress log

The log message is also avaiable as 'static'. In this case there is no running spinner
and every updates of the log result in a new line printed.

In this configuration the printed progress log lines are regular log lines, and
next printed logs are not displayed above the progress log.

The display style of the log can be either _normal_ (dynamic) or _static_.
To change between both display style, the function [`set_progress_static`]
can be called **when no progress logs are currently running.**

### Progress logs through log macros
_`(feature = "progress_through_logs")`_

This feature allows to manage progress logs using the `log` API macros.

In order to use progress logs, a specific syntax should be use in the logs.

```rust
#![cfg(feature = "progress_through_logs")]
use wirelog as log;

// Progress related logs starts with '[ ] '.
// (Notice the space after the closing bracket.)

// The logs that creates the progress ends with ':'
log::info!("[ ] This starts the progress log:");

// Subsequent logs updates the status:
log::info!("[ ] Status update 1");
log::info!("[ ] Status update 2");

// To end a progress logs, a character must be written
// in between the brackets:
// - '+' for success
// - '-' for failure
log::info!("[+] This is a success");
```

This functionnality allows to select on which level the progress
will be logged.
The level of the progress log corresponds to the level that has been
used for the creation of the progress.

This can be used for example to provide status updates only
using a lower log [`Level`]. The spinner will we displayed on
the appropriate [`LevelFilter`] and a more verbose output
with status updates might be provided using a even lower [`LevelFilter`].

